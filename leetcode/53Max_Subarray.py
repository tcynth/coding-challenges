from typing import List
import math

class Solution:
    def maxSubArray(self, A: List[int]) -> int:

        if not A:
            return 0

        if len(A) == 1:
            return A[0]


        S = [ -math.inf for x in A ]

        prev = A[0]
        S[0] = A[0]

        for i in range(1, len(A)):

            if A[i]  > S[i-1] +A[i]:
                S[i] = A[i]
            else:
                S[i] = A[i] + S[i-1]

        return max(S)


Sol = Solution()

print(Sol.max_subarray([1, 3, -9, 2, 4, 5]))
