""" 303. Range Sum Query - Immutable"""

from typing import List

class NumArray:

    def __init__(self, nums: List[int]):

        self.S = [ 0 for _ in range(len(nums) + 1) ]

        for i in range(1, len(nums) +1 ):
            self.S[i] = self.S[i-1] + nums[i-1]

    def sumRange(self, i: int, j: int) -> int:

        return self.S[j+1] - self.S[i]
