from typing import List

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution:


    def depth_first_search(self, r: TreeNode, a: List, totals : List[int]):

         if r.left:

            a.append(r.val)
            self.depth_first_search(r.left, a, totals)
            a.pop()

         if r.right:

            a.append(r.val)
            self.depth_first_search(r.right, a, totals)
            a.pop()

         if not r.left and not r.right:

            a.append(r.val)
            l = list(map(str, a))

            totals.append( int("".join(l), 2))

            a.pop()
            return None

    def sumRootToLeaf(self, root: TreeNode) -> int:

        bitsarr = []
        totals = []
        res = 0
        if not root:
            return res

        if not root.left and not root.right:
            res = root.val

        else:
            self.depth_first_search(root, bitsarr, totals )
            res = sum(totals)

        return res
