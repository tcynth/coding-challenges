from typing import List

class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None


class Solution:

    def level_order(self, root: TreeNode) -> List[List[int]]:

        res = []
        if not root:
            return res

        my_queue = [(root, 0)]

        while my_queue:
            (node, level) = my_queue.pop(0)

            if level == len(res):
                level_arr = [node.val]
                res.append(level_arr)

            elif res[level]:
                    res[level].append(node.val)

            if node.left:
                my_queue.append((node.left, level+1))
            if node.right:
                my_queue.append((node.right, level+1))

        return res
