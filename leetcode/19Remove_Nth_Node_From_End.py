# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next
class Solution:
    def removeNthFromEnd(self, head: ListNode, n: int) -> ListNode:

        if n == 0:
            return head

        c0 = 0
        c1 = 0

        first_cursor = head
        second_cursor = head

        while first_cursor.next:
            if c0 - c1 == n:
                second_cursor = second_cursor.next
                c1 += 1
            first_cursor = first_cursor.next
            c0 += 1

        if c0 == n-1:
            head = head.next
        elif n == 1:
            second_cursor.next = None
        else:
            second_cursor.next = second_cursor.next.next

        return head

S  = Solution()
e = None
# second = ListNode(3, e)

second = None

first = ListNode(2, second)
prev = ListNode(1, first)


#
# for i in range(7, 1 ,-1):
#     e = ListNode(i, prev)
#     print("hi")
#     prev = e

curr = prev
while curr:
    print(curr.val)
    curr = curr.next

r = S.removeNthFromEnd(prev, 2)
r_pt = r
while r_pt:
    print(r_pt.val)
    r_pt = r_pt.next
