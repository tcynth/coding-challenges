class Solution(object):
    def reconstructQueue(self, people):
        """
        :type people: List[List[int]]
        :rtype: List[List[int]]
        """

        res = [  ]

        if not people:
            return res

        sorted_queue = sorted(people,  key=lambda x:(-x[0], x[1]))

        height = sorted_queue[0][0]

        for e in sorted_queue:
            h, k = e
            if h == height:
                res.append(e)
            else:
                res.insert(k, e )

        return res


