class Solution:
    def climbStairs(self, n: int) -> int:
        if n <= 3:
            return n

        S = [ [0] * (n+1)  for i in range(3)]
        S[0] = [x for x in range(n+1)]
        S[1] = [1 for x in range(n+1)]
        for i in range(n+1):
            if i == 0:
                S[2][i] = 2
            elif i <= 2:
                S[2][i] = i
            else:
                S[2][i] = S[2][i-1] + S[2][i-2]


        return S[2][n]


