from typing import List
import math

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right
class Solution:
    def sortedArrayToBST(self, nums: List[int]) -> TreeNode:

        if not nums:
            return None

        if len(nums) <= 2:

            if len(nums) == 1:
                return TreeNode(nums[0])

            if len(nums) == 2:
                left = TreeNode(nums[0])
                root = TreeNode(nums[1])
                root.left = left
                return root


        mid_pos = math.floor(len(nums) /2 )
        root = TreeNode(nums[mid_pos])
        root.left = self.sortedArrayToBST(nums[: mid_pos])
        root.right = self.sortedArrayToBST(nums[mid_pos+1:])

        return root
