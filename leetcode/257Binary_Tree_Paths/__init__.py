# Definition for a binary tree node.
from typing import List

class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Solution:

    def all_paths_DFS(self, root: TreeNode, paths: List[str], r: List[int]):

        if root.left:
            r.append(root.val)
            self.all_paths_DFS(root.left, paths, r)
            r.pop()

        if root.right:
            r.append(root.val)
            self.all_paths_DFS(root.right, paths, r)
            r.pop()

        if not root.left and not root.right:
            r.append(root.val)
            paths.append("->".join([str(_) for _ in r]))
            r.pop()

        return paths


    def binaryTreePaths(self, root: TreeNode) -> List[str]:

        res = []
        if not root:
            return res

        self.all_paths_DFS(root, res, [])

        return res
