from typing import List

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution:

    def all_leaves_DFS(self, root: TreeNode, r: List[int]):

        if root.left:
            self.all_leaves_DFS(root.left, r)

        if root.right:
            self.all_leaves_DFS(root.right, r)

        if not root.left and not root.right:
            r.append(root.val)


    def leafSimilar(self, root1: TreeNode, root2: TreeNode) -> bool:
        if not root1 and not root2:
            return True

        if not root1 and root2:
            return False

        if not root2 and root1:
            return False

        res1 = []
        res2 = []
        self.all_leaves_DFS(root1, res1)
        self.all_leaves_DFS(root2, res2)

        return res1 == res2
