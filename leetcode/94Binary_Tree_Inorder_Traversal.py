from typing import List

class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None


class Solution:

    def in_order_next_node(self, root:TreeNode, res: List[int]) :

        if root:

            l = self.in_order_next_node(root.left, res)
            if l:
                res.append(l)

            res.append(root.val)

            n  = self.in_order_next_node(root.right, res)
            if n:
                res.append(n)

    def inorder_traversal(self, root: TreeNode) -> List[int]:

        res = []

        if not root:
            return res

        self.in_order_next_node(root, res)

        return res
