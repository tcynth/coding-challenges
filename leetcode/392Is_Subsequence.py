class Solution(object):
    def isSubsequence(self, s, t):

        if not s:
            return True

        c = len(t)
        r = len(s)

        found = 0

        start = 0
        for i in range(0, r):
            for j in range(start, c):
                if (t[j] == s[i]):
                    found += 1
                    start = j+1
                    break

        return found == len(s)


S = Solution()
s = "acd"
t = "ahbgdc"
# s = "letcode"

# t = "lyetyycyyoyydyye"
print(S.isSubsequence(s, t))
