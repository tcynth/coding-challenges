# Definition for a binary tree node.
class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution:
    def compareNodes(self, root: TreeNode, v: int) -> bool:

        if not root:
            return True

        if root.val != v:
            return False

        return self.compareNodes(root.left, v) and self.compareNodes(root.right, v)


    def isUnivalTree(self, root: TreeNode) -> bool:

        if not root:
            return False

        my_val = root.val

        return self.compareNodes(root.left, my_val) and self.compareNodes(root.right, my_val)
