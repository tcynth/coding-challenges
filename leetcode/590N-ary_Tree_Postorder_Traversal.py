from typing import List

# Definition for a Node.
class Node:
    def __init__(self, val=None, children=None):
        self.val = val
        self.children = children


class Solution:

    def getPostorder(self, current: 'Node', r:List[int]) :

        if current.children:
            for c in current.children:
                self.getPostorder(c, r)


        r.append(current.val)


    def postorder(self, root: 'Node') -> List[int]:

        res = []

        if not root:
            return res

        self.getPostorder(root, res)

        return res
