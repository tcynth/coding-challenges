class Solution(object):
    def countSquares(self, matrix):
        """
        :type matrix: List[List[int]]
        :rtype: int
        """

        r, c = len(matrix), len(matrix[0])
        count = 0

        for _ in matrix:
            print(_)

        S = [ [0] * (c+1) for _ in range(r+1) ]

        for i in range (1, r+1):
            for j in range (1, c+1):
                if matrix[i-1][j-1]:
                    S[i][j] = min(S[i-1][j-1], S[i-1][j], S[i][j-1]) + matrix[i-1][j-1]
                    count += S[i][j]

        for _ in S:
            print (_)

        return count

# matrix = [[0,0,0],[0,1,0],[0,1,0],[1,1,1],[1,1,0]]

matrix = [
  [0,1,1,1],
  [1,1,1,1],
  [0,1,1,1]
]

S = Solution()

print(S.countSquares(matrix))
