from typing import List

class Solution:
    def change(self, amount: int, coins: List[int]) -> int:

        if amount == 0:
                return 1

        if not coins :
            return 0

        M = [[0] * (amount+1) for x in range(len(coins)+1)]

        row0 = [ x for x in range(0, amount+1)]
        M[0] = row0

        row1 = []
        row1.append(coins[0])
        for x in range(1, amount+1):

            if x == row1[0] or x % row1[0] == 0:
                row1.append(1)
            else:
                row1.append(0)

        M[1] = row1

        for i in range(2, len(coins) +1):
            M[i][0] = coins[i-1]
            for j in (range(1, amount+1)):
                if M[i][0] == M[0][j]:
                    M[i][j] = M[i-1][j] +1
                elif  M[i][0] >= M[0][j]:
                    M[i][j] = M[i-1][j]
                else:
                    diff = M[0][j] - M[i][0]
                    M[i][j] =  M[i-1][j] +  M[i][diff]


        return M[len(coins)][amount]


S = Solution
S.change(5, [1,2,5])
