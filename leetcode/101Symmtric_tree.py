from typing import List

class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None


class Solution:

    def is_mirror(self, n1: TreeNode, n2:TreeNode) -> bool:

        if n1 is None and n2 is None:
            return True

        if None in (n1, n2):
            return False

        if n1.val != n2.val:
            return False

        return self.is_mirror(n1.left, n2.right) and self.is_mirror(n1.right, n2.left)

    def isSymmetric(self, root: TreeNode) -> bool:

        if root is None:
            return True

        return self.is_mirror(root, root)
