# Definition for a binary tree node.
class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None


class Solution:

    def rangeSumBST(self, root: TreeNode, L: int, R: int) -> int:

        res = 0

        if not root:
            return res

        if root.val < L:
            res += self.rangeSumBST(root.right, L, R)

        elif root.val > R:
            res += self.rangeSumBST(root.left, L, R)

        else:
            res += root.val
            if root.left:
                res += self.rangeSumBST(root.left, L, root.val - 1)
            if root.right:
                res += self.rangeSumBST(root.right, root.val, R)

        return res
