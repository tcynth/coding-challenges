from typing import List

# Definition for a Node.
class Node:
    def __init__(self, val=None, children=None):
        self.val = val
        self.children = children


class Solution:

    def getPreorder(self,  root:'Node', r:List[int]):

        if root.val:
            r.append(root.val)

        if root.children:
            for c in root.children:
                self.getPreorder(c, r)


    def preorder(self, root: 'Node') -> List[int]:

        res = []

        if not root:
            return res

        self.getPreorder(root, res)

        return res
