class Solution(object):

    def matrixBlockSum(self, mat, K):
        r, c = len(mat), len(mat[0])

        pre = [ [0] * ( c+1 )  for _ in range(r+1) ]

        for i in range(r):
            for j in range(c):
                pre[i+1][j+1] = pre[i+1][j] + pre[i][j+1] - pre[i][j] + mat[i][j]


        res = [ [0] * c for _ in range(r) ]

        for i in range(r):
            for j in range(c):
                c1, r1, c2, r2, = max(j-K, 0), max(i-K, 0), min(c,j+1+K ), min(r, i+1+K)
                res[i][j] = pre[r2][c2] - pre[r2][c1] - pre[r1][c2] + pre[r1][c1]

        return res


