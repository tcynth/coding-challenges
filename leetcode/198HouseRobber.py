from typing import List

class Solution:
    def rob(self, nums: List[int]) -> int:

        if not nums:
            return 0

        S = [ 0 for x in nums ]

        for i in range(len(nums)):

            if i == 0:
                S[i] = nums[i]

            elif i == 1:
                S[i] = max(S[i-1], nums[i])

            else:
                S[i] = max(nums[i] + S[i-2], S[i-1])

        return S[len(nums)-1]




Sol = Solution()

print(Sol.rob([1,2,3,1]))
