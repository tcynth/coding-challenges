from typing import List

# Definition for a Node.
class Node:

    def __init__(self, val=None, children=None):
        self.val = val
        self.children = children


class Solution:

    def maxDepth(self, root: 'Node') -> int:

        if not root:
            return 0

        if not root.children:
            return 1

        depth = []
        for c in root.children:
            depth.append(1+ self.maxDepth(c))

        return max(depth)

