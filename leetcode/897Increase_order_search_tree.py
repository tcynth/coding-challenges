from typing import List

# Definition for a binary tree node.
class TreeNode:

    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution:

    def in_order(self, r: TreeNode, l: List[TreeNode]):

        if not r:
            return

        self.in_order(r.left, l)
        l.append(r)
        self.in_order(r.right, l)

    def increasingBST(self, root: TreeNode) -> TreeNode:

        if not root:
            return None

        nodes = []

        self.in_order(root, nodes)
        for i in range(1, len(nodes)):
            nodes[i-1].left = None
            nodes[i-1].right = nodes[i]

        return nodes[0]
