from typing import List
import math
class Solution:

    def countBits(self, num: int) -> List[int]:

        S = [0]

        count = 1
        begin = 1
        diff = 1
        end = num +1

        for i in range(begin, end):
            if i == begin:
                begin = 2 ** (count)
                S.append(1)
                diff = 1
                count += 1
            else :
                S.append(1 + S[diff])
                diff += 1

        return S


S = Solution()
num = 5
print(S.countBits(num))

